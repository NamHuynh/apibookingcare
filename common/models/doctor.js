'use strict';

module.exports = function(Doctor) {
  Doctor.searchName = function(name, cb) {
    // eslint-disable-next-line max-len
    Doctor.find({where: {name: {like: '%' + name + '%'}}}, (_err, posts) => {
      var response = posts;
      cb(null, response);
    });
  };
  Doctor.remoteMethod(
    'searchName', {
      http: {path: '/searchName', verb: 'get'},
      // eslint-disable-next-line max-len
      accepts: {arg: 'name', type: 'string', required: true, http: {source: 'query'}},
      returns: {type: 'array', root: true},
    }
  );

  Doctor.searchNameBySpecialistId = function(name, specialistId, cb) {
    // eslint-disable-next-line max-len
    Doctor.find({where: {name: {like: '%' + name + '%'}, specialistId: specialistId}}, (_err, posts) => {
      var response = posts;
      cb(null, response);
    });
  };
  Doctor.remoteMethod(
    'searchNameBySpecialistId', {
      http: {path: '/searchNameBySpecialistId', verb: 'get'},
      // eslint-disable-next-line max-len
      accepts: [
        {arg: 'name', type: 'string', required: true, http: {source: 'query'}},
        {arg: 'specialistId', type: 'number', required: true, http: {source: 'query'}},
      ],
      returns: {type: 'array', root: true},
    }
  );
};
